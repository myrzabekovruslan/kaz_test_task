# kaz_test_task

Test task api.

## Description

For up server need to execute next commands:

```
cd cinema
docker-compose build
docker-compose up
```

Path: http://127.0.0.1:8000/swagger/

For insert data I described admin panel and
created superuser: "admin" with password "admin". 
You need to pass by path "/admin" and enter login and password.
