from rest_framework import serializers

from .models import Person, Actor, Director, Writer


class PersonSerializer(serializers.ModelSerializer):
    """Сериализатор для людей"""
    full_name = serializers.SerializerMethodField(source='get_full_name', label="Полное имя")

    def get_full_name(self, obj):
        return f'{obj.last_name} {obj.first_name}'

    class Meta:
        model = Person
        fields = "__all__"
        abstract = True


class ActorSerializer(PersonSerializer):
    """Сериализатор для актеров"""

    class Meta:
        model = Actor
        fields = ('full_name',)


class DirectorSerializer(PersonSerializer):
    """Сериализатор для режиссеров"""

    class Meta:
        model = Director
        fields = ('full_name',)


class WriterSerializer(PersonSerializer):
    """Сериализатор для сценаристов"""

    class Meta:
        model = Writer
        fields = ('full_name',)