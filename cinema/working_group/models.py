from django.db import models
from moving_pictures.models import Film, Serial


class Person(models.Model):
    """Человек"""
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    last_name = models.CharField(max_length=255, verbose_name="Фамилия")

    class Meta:
        abstract = True

    def __str__(self):
        return "{0} {1}".format(self.last_name, self.first_name)


class Actor(Person):
    """Актер"""
    films = models.ManyToManyField(
        Film,
        blank=True,
        related_name="actors",
        verbose_name="Фильмы",
    )
    serials = models.ManyToManyField(
        Serial,
        blank=True,
        related_name="actors",
        verbose_name="Сериалы",
    )

    class Meta:
        verbose_name = 'Актер'
        verbose_name_plural = 'Актеры'
        db_table = 'actors'


class Director(Person):
    """Режиссер"""
    films = models.ManyToManyField(
        Film,
        blank=True,
        related_name="directors",
        verbose_name="Фильмы",
    )
    serials = models.ManyToManyField(
        Serial,
        blank=True,
        related_name="directors",
        verbose_name="Сериалы",
    )

    class Meta:
        verbose_name = 'Режиссер'
        verbose_name_plural = 'Режиссеры'
        db_table = 'directors'


class Writer(Person):
    """Сценарист"""
    films = models.ManyToManyField(
        Film,
        blank=True,
        related_name="writers",
        verbose_name="Фильмы",
    )
    serials = models.ManyToManyField(
        Serial,
        blank=True,
        related_name="writers",
        verbose_name="Сериалы",
    )

    class Meta:
        verbose_name = 'Сценарист'
        verbose_name_plural = 'Сценаристы'
        db_table = 'writers'