from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--user', help="Admin's username")
        parser.add_argument('--pass', help="Admin's password")

    def handle(self, *args, **options):
        try:
            if not User.objects.filter(username=options.get('user')).exists():
                User.objects.create_superuser(
                    username=options.get('user'),
                    password=options.get('pass'),
                )
        except Exception as err:
            print(f"Command initadmin threw exception. {err}")