from django.apps import AppConfig


class MovingPicturesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'moving_pictures'
