from rest_framework import serializers

from .models import Film, Serial, Genre
from working_group.serializers import ActorSerializer, DirectorSerializer, WriterSerializer


class GenreSerializer(serializers.ModelSerializer):
    """Сериализатор для жанров"""

    class Meta:
        model = Genre
        fields = ('name', )


class FilmSerializer(serializers.ModelSerializer):
    """Сериализатор для фильмов"""
    genres = GenreSerializer(many=True)
    writers = WriterSerializer(many=True)
    actors = ActorSerializer(many=True)
    directors = DirectorSerializer(many=True)

    class Meta:
        model = Film
        exclude = ("id", )


class SerialSerializer(serializers.ModelSerializer):
    """Сериализатор для сериалов"""
    genres = GenreSerializer(many=True)
    writers = WriterSerializer(many=True)
    actors = ActorSerializer(many=True)
    directors = DirectorSerializer(many=True)

    class Meta:
        model = Serial
        exclude = ("id", )


class ResponseSerializer(serializers.Serializer):
    """Сериализатор для вывода фильмов и сериалов"""
    films = FilmSerializer(many=True)
    serials = SerialSerializer(many=True)