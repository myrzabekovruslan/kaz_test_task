from django.urls import path
from . import views

urlpatterns = [
    path('films_and_serials/', views.get_films_and_serials),
]