from django.db import models


class Genre(models.Model):
    """Жанр"""
    name = models.CharField(max_length=255, verbose_name="Название")
    description = models.CharField(max_length=1200, verbose_name="Описание")

    def __str__(self):
        return "{0}".format(self.name)

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'
        db_table = 'genres'


class MovingPicture(models.Model):
    """Кинокартина"""
    title = models.CharField(max_length=255, verbose_name="Заголовок")
    description = models.CharField(max_length=1200, verbose_name="Описание")
    create_date = models.DateTimeField(verbose_name="Дата создания")

    def __str__(self):
        return "{0}".format(self.title)

    class Meta:
        abstract = True


class Film(MovingPicture):
    """Фильм"""
    genres = models.ManyToManyField(
        Genre,
        blank=True,
        related_name="films",
        verbose_name="Жанры",
    )
    age_restriction = models.IntegerField(null=True, blank=True, verbose_name="Возрастной ценз")

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильм'
        db_table = 'films'


class Serial(MovingPicture):
    """Сериал"""
    genres = models.ManyToManyField(
        Genre,
        blank=True,
        related_name="serials",
        verbose_name="Жанры",
    )

    class Meta:
        verbose_name = 'Сериал'
        verbose_name_plural = 'Сериалы'
        db_table = 'serials'