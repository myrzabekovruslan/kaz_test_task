from rest_framework import viewsets, status, permissions
from rest_framework.decorators import action, api_view
from rest_framework.response import Response

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .serializers import FilmSerializer, SerialSerializer, ResponseSerializer
from .models import Film, Serial


@swagger_auto_schema(
    methods=['GET', ],
    operation_description="Получение списка всех фильмов и сериалов",
    responses={
        200: openapi.Response(
            'Формат ответа',
            schema=ResponseSerializer,
        )
    }
)
@api_view(['GET'])
def get_films_and_serials(request):
    films = FilmSerializer(Film.objects.all(), many=True)
    serials = SerialSerializer(Serial.objects.all(), many=True)
    data = {
        'films': films.data,
        'serials': serials.data,
    }
    return Response(data)